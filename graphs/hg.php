<?php

drupalize();
function drupalize() {
  while (!@stat('./includes/bootstrap.inc')) {
    chdir('..');
  }
  #module_load_include('/includes/bootstrap.inc', 'image', 'includes/bootstrap');
  require_once './includes/bootstrap.inc';
  drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL); // See http://drupal.org/node/211378#comment-924059
  #drupal_bootstrap(DRUPAL_BOOTSTRAP_DATABASE);
  #drupal_cron_run();
}

# Crucial - to suppress Devel (if installed and enabled) output appearing in the generated XML!
  $GLOBALS['devel_shutdown'] = FALSE;

# Get proper module path.
  $hgmodulepath = url(drupal_get_path('module', 'hypergraph'), array('absolute' => TRUE));

# url() ads i18n codes to the URL ... we need to remove them here...
/*
if ($langcode <> '') {
  $hgmodulepath = str_replace('/'. $langcode .'/', '/', $hgmodulepath);
}
*/

# Non-clean URLs need removing ?q=
$hgmodulepath = str_replace("?q=", "", $hgmodulepath);

if ($_GET['dtd'] == 1) {
  # Just supply the dtd.
  #ob_start();
  #echo 'http://quidne.vacilando.org' . $hgmodulepath . '/graphs/GraphXML.dtd';
  #echo $hgmodulepath;
  module_load_include('dtd', 'hg', 'GraphXML');
  #module_load_include('vacilando.org/sites/all/modules/hypergraph/graphs/GraphXML.dtd', 'hg', '/quidne');
  #$dtdfile = ob_get_clean();
  #echo $dtdfile;
  exit();
}


header("Content-type: text/xml");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

# Cache expiration time.
$hgcachexpire = 3600;  ### FIX THIS! ... was 3600
#global $user;
$userId = $user->uid;
$hgmenucacheid = 'hg_menu_'. md5($_SERVER['SERVER_NAME']).'_'.$userId .'_'. $_GET['par'];
/*
$begxml = '<?xml version="1.0"?><!DOCTYPE GraphXML SYSTEM "' . $hgmodulepath . 'GraphXML.dtd"><GraphXML><graph id="Quidne"><style><line tag="node" class="hpg0" colour="black"/><line tag="node" class="hpg1" colour="teal"/><line tag="node" class="hpg2" colour="red"/><line tag="node" class="hpg3" colour="blue"/><line tag="node" class="hpg4" colour="olive"/></style>';
*/

$begxml = '<?xml version="1.0"?><!DOCTYPE GraphXML SYSTEM "'. $hgmodulepath .'/graphs/hg.php?dtd=1"><GraphXML><graph id="Quidne"><style><line tag="node" class="hpg0" colour="black"/><line tag="node" class="hpg1" colour="teal"/><line tag="node" class="hpg2" colour="red"/><line tag="node" class="hpg3" colour="blue"/><line tag="node" class="hpg4" colour="olive"/></style>';
$endxml = '</graph></GraphXML>';

/*
$fullxml = '<?xml version="1.0"?><!DOCTYPE GraphXML SYSTEM "http://quidne.vacilando.org/sites/all/modules/hypergraph/graphs/hg.php?dtd=1"><GraphXML><graph id="Quidne"><style><line tag="node" class="hpg0" colour="black"/><line tag="node" class="hpg1" colour="teal"/><line tag="node" class="hpg2" colour="red"/><line tag="node" class="hpg3" colour="blue"/><line tag="node" class="hpg4" colour="olive"/></style><node class="hpg1" name="1"><label>Navigation</label></node><node class="hpg4" name="326"><label>Gallery of galleries</label></node><node class="hpg3" name="125"><label>Brilliant Gallery</label></node><edge source="125" target="326" /><node class="hpg4" name="327"><label>Gallery of hypergraphs</label></node><node class="hpg3" name="322"><label>Hypergraph</label></node><edge source="322" target="327" /><node class="hpg2" name="320"><label>Drupal</label></node><edge source="320" target="125" /><edge source="320" target="322" /><node class="hpg2" name="283"><label>Vacilando.org</label></node><node class="hpg2" name="57"><label>Recent posts</label></node><node class="hpg2" name="304"><label>Stats</label></node><node class="hpg4" name="303"><label>Science</label></node><node class="hpg3" name="59"><label>Categories</label></node><edge source="59" target="303" /><node class="hpg3" name="58"><label>Sources</label></node><node class="hpg2" name="35"><label>Aggregator</label></node><edge source="35" target="59" /><edge source="35" target="58" /><node class="hpg2" name="42"><label>Contact</label></node><node class="hpg1" name="124"><label>Navigation</label></node><edge source="124" target="320" /><edge source="124" target="283" /><edge source="124" target="57" /><edge source="124" target="304" /><edge source="124" target="35" /><edge source="124" target="42" /><node class="hpg1" name="2"><label>Primary links</label></node><node class="hpg0" name="0"><label>root</label></node><edge source="0" target="1" /><edge source="0" target="124" /><edge source="0" target="2" /></graph></GraphXML>';
           '<?xml version="1.0"?><!DOCTYPE GraphXML SYSTEM "http://quidne.vacilando.org/sites/all/modules/hypergraph/graphs/GraphXML.dtd"><GraphXML><graph id="Quidne"><style><line tag="node" class="hpg0" colour="black"/><line tag="node" class="hpg1" colour="teal"/><line tag="node" class="hpg2" colour="red"/><line tag="node" class="hpg3" colour="blue"/><line tag="node" class="hpg4" colour="olive"/></style><node class="hpg1" name="1"><label>Navigation</label></node><node class="hpg4" name="326"><label>Gallery of galleries</label><dataref><ref xlink:href="node/4" /></dataref></node><node class="hpg3" name="125"><label>Brilliant Gallery</label><dataref><ref xlink:href="node/3" /></dataref></node><edge source="125" target="326" /><node class="hpg4" name="327"><label>Gallery of hypergraphs</label><dataref><ref xlink:href="node/8" /></dataref></node><node class="hpg3" name="322"><label>Hypergraph</label><dataref><ref xlink:href="node/6" /></dataref></node><edge source="322" target="327" /><node class="hpg2" name="320"><label>Drupal</label><dataref><ref xlink:href="drupal" /></dataref></node><edge source="320" target="125" /><edge source="320" target="322" /><node class="hpg2" name="283"><label>Vacilando.org</label><dataref><ref xlink:href="http://www.vacilando.org" /></dataref></node><node class="hpg2" name="57"><label>Recent posts</label><dataref><ref xlink:href="tracker" /></dataref></node><node class="hpg2" name="304"><label>Stats</label><dataref><ref xlink:href="hof" /></dataref></node><node class="hpg4" name="303"><label>Science</label><dataref><ref xlink:href="aggregator/categories/1" /></dataref></node><node class="hpg3" name="59"><label>Categories</label><dataref><ref xlink:href="aggregator/categories" /></dataref></node><edge source="59" target="303" /><node class="hpg3" name="58"><label>Sources</label><dataref><ref xlink:href="aggregator/sources" /></dataref></node><node class="hpg2" name="35"><label>Aggregator</label><dataref><ref xlink:href="aggregator" /></dataref></node><edge source="35" target="59" /><edge source="35" target="58" /><node class="hpg2" name="42"><label>Contact</label><dataref><ref xlink:href="contact" /></dataref></node><node class="hpg1" name="124"><label>Navigation</label></node><edge source="124" target="320" /><edge source="124" target="283" /><edge source="124" target="57" /><edge source="124" target="304" /><edge source="124" target="35" /><edge source="124" target="42" /><node class="hpg1" name="2"><label>Primary links</label></node><node class="hpg0" name="0"><label>root</label></node><edge source="0" target="1" /><edge source="0" target="124" /><edge source="0" target="2" /></graph></GraphXML>
echo $fullxml; exit();
*/


#if ( $_SERVER['SERVER_ADDR'] == '64.13.192.90' ) {
if (variable_get('hypergraph_cache', 'd') == 'f') {
  #echo '.....................' . $_SERVER['SERVER_ADDR'];
  $temp = filecaching_wrapper_hgm();
}
else {
  $temp = caching_wrapper_hgm();
}

#echo $hgmenucacheid;
/*
$tstart = time();
do {
					sleep(1); # Give MySQL the time to write the data to the cache (if unfinished)
					$r = db_query( "SELECT * FROM cache WHERE `cid` = '" . $hgmenucacheid . "'" ) or die(mysql_error());
					$appletdata = '';
					while( $h = db_fetch_object($r) ) {
												$temp = $h->data;
												$appletdata = unserialize( $temp );
										}
			} while ( ( ( time() - $tstart ) < 10 ) and $temp == '' ); # Search for the expected cache (there can be delay) but no longer than 60 seconds.
*/

if ($temp <> '') {
  echo $temp;
}
else {
  echo $begxml;
  echo '<node name="1"><label>just</label></node>
											<node name="2"><label>a</label></node>
											<node name="3"><label>silly</label></node>
											<node name="4"><label>demo</label></node>
											<edge source="1" target="2" />
											<edge source="1" target="3" />
											<edge source="1" target="4" />
											<edge source="4" target="3" />
											<edge source="4" target="2" />';
  echo $endxml;
}
# IMPORTANT - otherwise some process after Hypergraph adds strings and breaks the XML!
exit();
function filecaching_wrapper_hgm($reset = FALSE) {
  global $hgcachexpire;
  global $hgmenucacheid;
  global $begxml;
  global $endxml;
  #echo $hgmenucacheid;
  $cachedfile = file_directory_temp() .'/'. $hgmenucacheid;
  $lastchanged = filectime($cachedfile);
  if ($lastchanged === false or (time() - $lastchanged > ($hgcachexpire))) {
    #echo '. 1.... ';
    # Cache file does not exist or is too old.
    $my_data = $begxml . hypergraph_get_menu(NULL) . $endxml;
    # Now put $my_data to cache!
    $fh = fopen($cachedfile, "w+");
    fwrite($fh, $my_data);
    fclose($fh);
  }
  else {
    #echo '. 2.... ';
    # Cache file exists.
    $my_data = file_get_contents($cachedfile);
  }
  return $my_data;
}

function caching_wrapper_hgm($reset = FALSE) {
  global $hgcachexpire;
  global $hgmenucacheid;
  global $begxml;
  global $endxml;
  #echo $hgmenucacheid;
  static $my_data;
  #echo '0.... ';
#      echo mt_rand(100);
  if (!isset($my_data) || $reset) {
    if (!$reset and ($cache = cache_get($hgmenucacheid)) and !empty($cache->data)) {
      $my_data = $cache->data;
      #echo '1.... ';# . $my_data;
    }
    else {
      // Do your expensive calculations here, and populate $my_data
      // with the correct stuff..
      $my_data = $begxml . hypergraph_get_menu(NULL) . $endxml;
      cache_set($hgmenucacheid, $my_data, 'cache', (time()+24*3600)); // Expire in 24 hours.
    }
  }
  return $my_data;
}

function array_searchRecursive($needle, $haystack, $strict = false, $path = array()) {
  # http://be2.php.net/manual/en/function.array-search.php
  # Searches haystack for needle and returns an array of the key path if it is found in the (multidimensional) array, FALSE otherwise.
  # mixed array_searchRecursive ( mixed needle, array haystack [, bool strict[, array path]] )
  if (!is_array($haystack)) {
    return false;
  }
  foreach ($haystack as $key => $val) {
    if (is_array($val) && $subPath = array_searchRecursive($needle, $val, $strict, $path)) {
      $path = array_merge($path, array($key), $subPath);
      return $path;
    }
    elseif ((!$strict && $val == $needle) || ($strict && $val === $needle)) {
      $path[] = $key;
      return $path;
    }
  }
  return false;
}

function findepth($needle, $haystack) {
  $foundepth    = 0;
  $foundparent  = NULL;
  $searchparent = false;
  do {
    $searchparent = array_searchRecursive($needle, $haystack);
    if ($searchparent !== false) {
      $foundepth++;
      unset($tempar[$searchparent]);
      $needle = $searchparent[0];
    }
    #echo '... ' . $foundepth . "\n";
  } while ($searchparent !== false);
  return $foundepth;
}

function hypergraph_get_menu($beginfrom) {
  $result = '';
  #print_r( menu_get_menu() );
  #print_r( $menu['visible'] ); # The returned structure contains much information that is useful only internally in the menu system. External modules are likely to need only the ['visible'] element of the returned array. All menu items that are accessible to the current user and not hidden will be present here, so modules and themes can use this structure to build their own representations of the menu.
  /*
  [visible] => Array
          (
              [195] => Array
                  (
                      [title] => Latest images
                      [path] => image/recent
                      [children] => Array
                          (
                          )

                      [type] => 22
                      [pid] => 1
                  )

              [125] => Array
                  (
                      [title] => weblinks
                      [path] => links/weblink
                      [children] => Array
                          (
                          )

                      [type] => 22
                      [pid] => 1
                  )
  */
  /*
  ["primary-links:0"]=>
  string(15) "<Primary links>"
  ["primary-links:1099"]=>
  string(7) "-- Root"
  ["primary-links:1106"]=>
  string(9) "---- test"
  ["primary-links:1100"]=>
  string(10) "-- Contact"
  ["secondary-links:0"]=>
  string(17) "
  */
  // Generate a list of possible parents (not including this item or descendants).
  #$item['mlid'] = 0;
  $menu = menu_parent_options(menu_get_menus(), $item);
  $selectedmenu = variable_get('hypergraph_menu_name', 'primary-links');
  $selectedmenu .= ':';
  /*
  foreach($menu as $key=>$val){
    if(substr($key,0,strlen($selectedmenu))==$selectedmenu){
      $ekko = explode(':',$key);
      echo $ekko[1]."\n";
      $lilo = menu_link_load($ekko[1]);
      $hlbka = explode(' ',$val); # E.g. string(9) "---- test" = 2nd under root
      $hlbka = (strlen($hlbka[0])/2);
      echo $lilo['title'] . ', path: ' . $lilo['link_path'] . ', depth: '.$hlbka. ', parent: ' . $lilo['plid']."\n";
    }
  }
  */
  
  foreach($menu as $key=>$val){
    if(substr($key,0,strlen($selectedmenu))==$selectedmenu){
      $ekko = explode(':',$key);
      #echo $ekko[1]."\n";
      if ($ekko[1]==0) {# Means it's the root of this menu
        $lilo['title'] = $_SERVER['SERVER_NAME'];
        $lilo['link_path'] = '';
        $lilo['plid'] = '';
        $hlbka = 0;
      } else {
        $lilo = menu_link_load($ekko[1]);
        $hlbka = explode(' ',$val); # E.g. string(9) "---- test" = 2nd under root
        $hlbka = (strlen($hlbka[0])/2);
      }
      #echo "\n".$lilo['title'] . ', path: ' . $lilo['link_path'] . ', depth: '.$hlbka. ', parent: ' . $lilo['plid']."\n";
      
      $result .= '<node class="hpg'. $hlbka .'" name="'. $ekko[1] .'"><label>'. $lilo['title'] .'</label>';
      if ($lilo['link_path'] <> '') {
        $result .= '<dataref><ref xlink:href="'. $lilo['link_path'] .'" /></dataref>';
      }
      $result .= '</node>';
      if($lilo['plid']<>'') { # '' means it's menu root so no parent.
        $result .= '<edge source="'. $ekko[1] .'" target="'. $lilo['plid'] .'" />';
      }
      # . "\n";
      
    }
  }
  
  /*
  echo "\n\n...............\n\n";
  var_dump($menu);
  $temparx = $menu['primary-links:0'];
  $tempar  = NULL;
  foreach ($temparx as $key => $value) {
    foreach ($value['children'] as $key2 => $value2) {
      $tempar[$key][$key2] = $value2;
    }
  }
  foreach ($menu['visible'] as $key => $value) {
    if ($value['title'] == '') {
      $value['title'] = 'root';
    }
    echo '<node class="hpg'. findepth($key, $tempar) .'" name="'. $key .'"><label>'. $value['title'] .'</label>';
    if ($value['path'] <> '') {
      echo '<dataref><ref xlink:href="'. $value['path'] .'" /></dataref>';
    }
    # . "\n";
    echo '</node>';
    foreach ($value['children'] as $key2 => $value2) {
      #\n';
      echo '<edge source="'. $key .'" target="'. $value2 .'" />';
    }
  }
  */
  #print_r( menu_tree() );
  #print_r( menu_secondary_links() );
  #$temp = ob_get_clean();
  ///*
  #$temp = '<node class="hpg0" name="0"><label>big bang</label></node><node class="hpg1" name="393"><label>WritingsS</label><dataref><ref xlink:href="writings" /></dataref><edge source="393" target="0" /></node>';
  #$result = '<node class="hpg2" name="195"><label>Latest images</label><dataref><ref xlink:href="image/recent" /></dataref></node><node class="hpg2" name="125"><label>weblinks</label><dataref><ref xlink:href="links/weblink" /></dataref></node><node class="hpg1" name="1"><label>Navigation</label></node><edge source="1" target="195" /><edge source="1" target="125" /><node class="hpg2" name="169"><label>Home</label><dataref><ref xlink:href="node/1" /></dataref></node><node class="hpg2" name="119"><label>Photos</label><dataref><ref xlink:href="node/45" /></dataref></node><node class="hpg2" name="168"><label>Blog</label><dataref><ref xlink:href="node/5" /></dataref></node><node class="hpg1" name="117"><label>Menu</label></node><edge source="117" target="169" /><edge source="117" target="119" /><edge source="117" target="168" /><node class="hpg2" name="118"><label>Photos</label><dataref><ref xlink:href="node/45" /></dataref></node><node class="hpg2" name="156"><label>Blog</label><dataref><ref xlink:href="node/5" /></dataref></node><node class="hpg2" name="157"><label>Links</label><dataref><ref xlink:href="links/weblink" /></dataref></node><node class="hpg2" name="155"><label>AAI</label><dataref><ref xlink:href="node/2" /></dataref></node><node class="hpg2" name="193"><label>Clock</label><dataref><ref xlink:href="node/49" /></dataref></node><node class="hpg2" name="158"><label>Contact</label><dataref><ref xlink:href="contact" /></dataref></node><node class="hpg1" name="2"><label>Primary links</label></node><edge source="2" target="118" /><edge source="2" target="156" /><edge source="2" target="157" /><edge source="2" target="155" /><edge source="2" target="193" /><edge source="2" target="158" /><node class="hpg0" name="0"><label>root</label></node><edge source="0" target="1" /><edge source="0" target="117" /><edge source="0" target="2" />';
  #$temp = '................';
  //*/
  return $result;
}

# Some people don't have PHP5 yet...
function microtime_float() {
  list($usec, $sec) = explode(" ", microtime());
  return ((float)$usec + (float)$sec);
}
