
Integration of Java hyperbolic tree geometry visualization developed at http://hypergraph.sourceforge.net/

Installation, demo and tips are all at http://vacilando.net/hg


Author: Tomas Fulopp (Vacilando)

If you use this module, find it useful, send me a message via http://vacilando.net/contact (and don't forget to include a link to your gallery!)

The author can also be contacted for paid customizations of this and other modules.

Development of this module is sponsored by Vacilando.org ( http://www.vacilando.org/ )
